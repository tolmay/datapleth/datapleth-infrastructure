#!/bin/bash

# ---------------------------------------------------
# Terraform apply destroy job
# - based on environment variables
# - see bash_profile
# ---------------------------------------------------
terraform apply terraform.tfplan
