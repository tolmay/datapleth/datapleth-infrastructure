#!/bin/bash

# ---------------------------------------------------
# Terraform plan job for destroying
# - based on environment variables
# - see bash_profile
# ---------------------------------------------------
terraform plan -destroy -out=terraform.tfplan  \
       -var "scaleway_pub_key=$HOME/.ssh/id_rsa.pub" \
       -var "scaleway_ssh_fingerprint=${SCW_SSH_FINGERPRINT}"