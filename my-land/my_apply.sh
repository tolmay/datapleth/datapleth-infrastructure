#!/bin/bash

# ---------------------------------------------------
# Terraform apply job
# - based on environment variables
# - see bash_profile
# ---------------------------------------------------
terraform apply  \
       -var "scaleway_pub_key=${HOME}/.ssh/id_rsa.pub" \
       -var "scaleway_ssh_fingerprint=$SCW_SSH_FINGERPRINT"