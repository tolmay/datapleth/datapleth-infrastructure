#!/bin/bash

# ---------------------------------------------------
# Terraform plan job
# - based on environment variables
# - see bash_profile
# ---------------------------------------------------
terraform plan  \
       -var "scaleway_pub_key=$HOME/.ssh/id_rsa.pub" \
       -var "scaleway_ssh_fingerprint=${SCW_SSH_FINGERPRINT}"
