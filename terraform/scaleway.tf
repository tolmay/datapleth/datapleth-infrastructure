# # Create Public IP, it's not included in scaleway contrary to digitalocean
# resource "scaleway_instance_ip" "public_ip" {}



# # Add security group to ensure firewalling
# # accept ssh, http and https
# resource "scaleway_instance_security_group" "datapleth-security-group" {
#   inbound_default_policy  = "drop"
#   outbound_default_policy = "accept"

#   inbound_rule {
#     action = "accept"
#     port   = "22"
#   }

#   inbound_rule {
#     action = "accept"
#     port   = "8080"
#   }

#   inbound_rule {
#     action = "accept"
#     port   = "443"
#   }
# }



## Try K8S
resource "scaleway_k8s_cluster" "datapleth-k8s" {
  name = "datapleth-k8s"
  version = "1.22.1"
  cni = "weave"
}

resource "scaleway_k8s_pool" "datapleth-pool" {
  cluster_id = scaleway_k8s_cluster.datapleth-k8s.id
  name = "datapleth-pool"
  node_type = "DEV1-M"
  size = 1
  autoscaling = true
  autohealing = true
  min_size = 1
  max_size = 3
}


resource "local_file" "kubeconfig" {
  content = scaleway_k8s_cluster.datapleth-k8s.kubeconfig[0].config_file
  filename = "kubeconfig"
}

resource "scaleway_rdb_instance" "keycloack" {
  name           = "keycloack"
  node_type      = "DB-DEV-S"
  engine         = "MySQL-8"
  is_ha_cluster  = false
  disable_backup = true
}
output "cluster_url" {
  value = scaleway_k8s_cluster.datapleth-k8s.apiserver_url
}

