# datapleth infrastructure

Repo to gather terraform and scripts to build datapleth analytics platform infrastructure



## Pre-requisistes

### Softwares

- Terraform need to be isntalled, refer to terraform documentation
- git need to be installed and configured with ssh

### Credentials as env variable

scaleway credential should be added in .bash_profile_datapleth file as environement variable to avoid having such keys in code.

Your .bash_profile_datapleth should looks like the file bellow, we are using the standard env variable used
by the scaleway terraform provider.

AWS_* credential are used by S3 object storage of scaleway, they are
identical to your scaleway credentials.

`source .bash_profile_datapleth` from your terminal to export variable.

```
## ========== Scaleway elements credentials ========
export SCW_ACCESS_KEY="xxx..."
export SCW_SECRET_KEY="xxx..."
export AWS_ACCESS_KEY_ID="xxx..."
export AWS_SECRET_ACCESS_KEY="xxx..."
export SCW_DEFAULT_PROJECT_ID="xxx..."
export SCW_SSH_FINGERPRINT="xxx..."

```

### Backend to store terraform state

An object storage should be created beforehead one scaleway and configured as provider.


## Create your K8S Kapsule

### Init your terraform providers.

add you key 
`ssh-add ~/.ssh/id_your_key`

`terraform init`


## References 

We used this tutorial : 
https://www.scaleway.com/en/docs/deploy-your-first-terraform-infrastructure-on-scaleway/

And this :
https://blog.wescale.fr/2020/09/16/une-forge-logicielle-gitlab-kubernetes-hebergee-chez-scaleway-totalement-en-infrastructure-as-code/

For K8s :
https://particule.io/blog/scaleway-kapsule/